var map = L.map('mapid', {
    center: [42.2667, 2.9667],
    zoom: 13
});

map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
map.boxZoom.disable();
map.keyboard.disable();

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

// L.marker([42.266950, 2.956106]).addTo(map);
// L.marker([42.273579, 2.946275]).addTo(map);
// L.marker([42.268114, 2.959612]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            var marcador = L.marker(bici.ubicacion, {title: bici.code}).addTo(map);
            marcador.bindPopup(`<b>Bicicleta Code: ${bici.code}</b><br>Modelo: ${bici.modelo}<br>Color: ${bici.color}`).openPopup();
        })
    }
})
