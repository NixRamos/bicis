const nodemailer = require('nodemailer')
const sgTransport = require('nodemailer-sendgrid-transport')

// const mailConfig = {
//     host: process.env.MAIL_HOST,
//     port: process.env.MAIL_PORT,
//     auth: {
//         user: process.env.MAIL_USERNAME,
//         pass: process.env.MAIL_PASSWORD
//     },
// }

const mailConfig = {
    host: "smtp.ethereal.email",
    port: 587,
    auth: {
        user: process.env.ETHEREAL_USER_DEV,
        pass: process.env.ETHEREAL_PASS_DEV
    },
}

module.exports = nodemailer.createTransport(mailConfig)